import {qustn} from '../components/question/questions';
const questionReducer = (state:any = {
    currentQuestion:1, 
    totalQuestion:qustn.length, 
    answeredQuestion:[], 
    questions: qustn
}, action:any) => {
    switch(action.type){
        case "DECREMENT":
            if (state.currentQuestion > 1){
                return state={...state, currentQuestion: state.currentQuestion-1}
            }
            else{
                return state={...state, currentQuestion: state.currentQuestion}
            }
        case "INCREMENT":
            if (state.currentQuestion < qustn.length){
                return state={...state, currentQuestion: state.currentQuestion+1}
            }
            else{
                return state={...state, currentQuestion: state.currentQuestion}
            }
        case "AQ":
            if(action.value){
                return state={...state, answeredQuestion:[...state.answeredQuestion, action.value]};
            }
            else{
                return state={...state};
            }
        case "GET_QUSTN":

                return state={...state, questions: state.questions}
            
        default:
            return state={...state}
    }
}
export default questionReducer;