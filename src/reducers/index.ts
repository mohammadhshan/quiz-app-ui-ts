import questionReducer from './questionReducer';
import {combineReducers} from 'redux';

interface reducerState{
    counter: any,
} 

const allReducers = combineReducers<reducerState>({
    counter: questionReducer,
});

export default allReducers;

