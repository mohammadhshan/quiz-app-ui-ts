import React from 'react';
import './dashboard.scss';
import SubHeaderComponent from '../../components/sub-header/subHeader.component';
import CollegeList from '../../components/college/college-list';
import { Route } from 'react-router-dom';


const Dashboard = (props: any) => {
    return (
        <div>
            <SubHeaderComponent/>
            <Route path="/dashboard/college" component={CollegeList}></Route>
        </div>
    )
}

export default Dashboard;
