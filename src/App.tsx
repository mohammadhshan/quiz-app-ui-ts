import React from 'react';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/common.scss';
import Layout from './components/layout/layout';
import Home from './pages/home/home';
import {
  Switch,
  Route,
  BrowserRouter,
} from "react-router-dom";
import Dashboard from './pages/dashboard/dashboard';

function App() {
  
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Layout>
            <Route exact path="/" component={Home} />
            <Route path="/dashboard" component={Dashboard}/>
          </Layout>
        </Switch>
      </BrowserRouter>
    </div>
  );

}
export default App;