import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './subHeader.component.scss';
import { Container, Button } from 'react-bootstrap';
import { FaUsers, FaUniversity, FaList } from "react-icons/fa";
import { IoIosAddCircle } from "react-icons/io";
import testIcon from '../../assets/image/test.png';
import resutIcon from '../../assets/image/result.png';
import qstnIcon from '../../assets/image/question.png';
import examIcon from '../../assets/image/exam.png';
import AddCollegeModal from '../modals/add-college/add-college-modal';

const SubHeaderComponent = (props: any) => {

const [rightLink, setRightLink] = useState<string>("Colleges"); 

const [AddCollegeModalShow, setAddCollegeModalShow] = useState(false);

const linkAddRight = (e:any) =>{
    setRightLink(e.target.text);
}
    
    return (
        <div className="sub-header-wrapper">
            <Container fluid className="px-lg-50">
                <div className="sublink-container d-flex">
                    <div className="sublink-left">
                        <ul>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/test">
                                    <img src={testIcon} alt="Test Icon"></img>Test
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/add-question">
                                    <img src={qstnIcon} alt="Question Icon"></img>Add Question
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/users">
                                    <i><FaUsers/></i>Users
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/category">
                                    <i><FaList/></i>Category
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/test-result">
                                    <img src={resutIcon} alt="Result Icon"></img>Test Result
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/college">
                                    <i><FaUniversity/></i>Colleges
                                </NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="is-active" onClick={(e:any)=>{linkAddRight(e)}} to="/dashboard/exams">
                                    <img src={examIcon} alt="Exam Icon"></img>Exams
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                    <div className="sublink-right">
                        <ul>
                            <li>
                                {rightLink === "Colleges" && <Button variant="link" onClick={(e:any)=>setAddCollegeModalShow(true)}><i><IoIosAddCircle/></i>Add Colleges</Button>}
                                {rightLink === "Exams" && <Button variant="link"><i><IoIosAddCircle/></i>Add Exams</Button>}
                                {rightLink === "Add Question" && <Button variant="link"><i><IoIosAddCircle/></i>Add Question</Button>}
                            </li>
                        </ul>
                    </div>
                </div>
            </Container>
            <AddCollegeModal show={AddCollegeModalShow} onHide={() => setAddCollegeModalShow(false)}/>
        </div>
    )
}

export default SubHeaderComponent;