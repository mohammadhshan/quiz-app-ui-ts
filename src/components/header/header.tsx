import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import './header.scss';
import { FaUserAlt } from "react-icons/fa";

const Header = (props: any) => {
    return (
        <Navbar variant="light" className="navbar header-bg px-lg-50">
            <Navbar.Brand href="/" className="text-white">NeoQuizApp</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Nav className="ml-auto">
<p className="user-info text-white">Hi, <span>User Name</span><i className="user-icon"><FaUserAlt/></i></p>
                <div className="text-white"><i className="logout-icon"></i></div>
            </Nav>
        </Navbar>
    )
}

export default Header