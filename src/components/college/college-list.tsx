import React from 'react';
import './college-list.scss';
import Table from 'react-bootstrap/Table'
import Pagination from 'react-bootstrap/Pagination';
import { FaRegEdit, FaTrashAlt } from 'react-icons/fa';
import { BsChevronRight, BsChevronLeft, BsChevronBarRight, BsChevronBarLeft} from "react-icons/bs";

const removeListRow = (e:any) => {
    e.target.parentNode.parentNode.remove();
}

const CollegeList = (props: any) => {
  
    return (
        <div>
            <div className="main-wrapper">
                <div className="main-container">
                    <div className="content-container">
                        <div className="table-list-wrapper">
                            <Table responsive="md">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Govt College Attingal</td>
                                        <td>4-(331)735-1276</td>
                                        <td>cgomez@blogtags.gov</td>
                                        <td>Attingal, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Iqbal College</td>
                                        <td>2-(229)515-8474</td>
                                        <td>rdixon@wikido.mil</td>
                                        <td>Peringammala, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>University College</td>
                                        <td>4-(331)735-1276</td>
                                        <td>university@blogtags.gov</td>
                                        <td>Palayam, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sree Narayana College</td>
                                        <td>8-(331)735-1276</td>
                                        <td>sn@blogtags.gov</td>
                                        <td>Chembazhanthy, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Govt College Attingal</td>
                                        <td>4-(331)735-1276</td>
                                        <td>cgomez@blogtags.gov</td>
                                        <td>Attingal, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Iqbal College</td>
                                        <td>2-(229)515-8474</td>
                                        <td>rdixon@wikido.mil</td>
                                        <td>Peringammala, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>University College</td>
                                        <td>4-(331)735-1276</td>
                                        <td>university@blogtags.gov</td>
                                        <td>Palayam, Trivandrum</td>
                                        <td className="options-cell">
                                            <span className="edit-icon"><i><FaRegEdit/></i>Edit</span> 
                                            <span className="seperator">|</span> 
                                            <span className="delete-icon" onClick={(e:any)=>{removeListRow(e)}}><i><FaTrashAlt/></i>Delete</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </div>
                </div>
                <div className="pagination-container">
                    <Pagination>
                        <Pagination.First><BsChevronBarLeft/></Pagination.First>
                        <Pagination.Prev><BsChevronLeft/></Pagination.Prev>
                        <Pagination.Item active>{1}</Pagination.Item>
                        <Pagination.Item>{2}</Pagination.Item>
                        <Pagination.Item>{3}</Pagination.Item>
                        <Pagination.Item>{4}</Pagination.Item>
                        <Pagination.Item>{5}</Pagination.Item>
                        <Pagination.Next><BsChevronRight/></Pagination.Next>
                        <Pagination.Last><BsChevronBarRight/></Pagination.Last>
                    </Pagination>
                </div>
            </div>
        </div>
    )
}

export default CollegeList;
