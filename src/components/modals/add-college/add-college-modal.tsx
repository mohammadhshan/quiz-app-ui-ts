import React from 'react';
import { Modal, Button, Container, Row, Col } from 'react-bootstrap';
import './add-college-modal.scss';

const AddCollegeModal = (props: any) => {
    return (
        <Modal {...props} animation={true} size="lg" dialogClassName="addCollegeModal-wrapper"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
            <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Add New College</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container className="paddingLR-0">
                        <Row className="frm-row-modal">
                            <Col xs={12} md={4}>
                                <label htmlFor="name">Name</label>
                                <input type="text" id="name"></input>
                            </Col>
                            <Col xs={12} md={4}>
                                <label htmlFor="email">E-mail</label>
                                <input type="text" id="email"></input>
                            </Col>
                            <Col xs={12} md={4}>
                                <label htmlFor="phoneNumber">Phone Number</label>
                                <input type="text" id="phoneNumber"></input>
                            </Col>
                        </Row>
                        <Row className="frm-row-modal">
                            <Col xs={12}>
                                <label htmlFor="address">Address</label>
                                <textarea id="address" placeholder="Placeholder text (Hint text)"></textarea>
                            </Col>
                        </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
            <Button className="modal-btn-cancel" onClick={props.onHide}>
                Cancel
            </Button>
            <Button className="modal-btn-submit" onClick={props.onHide}>
                Submit
            </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default AddCollegeModal;
