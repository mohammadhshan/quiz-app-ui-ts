import React from 'react';
import './overview.component.scss';
import { Button } from 'react-bootstrap';
import {useSelector} from 'react-redux';

const OverviewComponent = () => {

    const qustnCount = useSelector((state:any) => state.counter.currentQuestion);

    const qustnCountTotal = useSelector((state:any) => state.counter.totalQuestion);

    const answeredQstnArray= useSelector((state:any) => state.counter.answeredQuestion);

    const qustnCountBlocks= new Array(qustnCountTotal);
    qustnCountBlocks.fill(1)
    const QuestionNumbers = qustnCountBlocks.map((q,index)=>{
        let classname = index === (qustnCount-1) ? "qstn-cq" : answeredQstnArray.indexOf(index+1) >= 0 ? "qstn-answered":'';
        return <span key={index} className={classname}>{index + 1}</span>;
    });


    return (
        <div className="col px-lg-50 py-lg-40 py-md-15 qstn-overview-container">
            <div className="qstn-overview-title">
                Question Overview
            </div>
            <div className="overview-num-wrapper" id="overview-qnum">
                {QuestionNumbers}
                {/* Conditional span classNames are given below*
                    qstn-cq for Current Question
                    qstn-answered for Answered Question
                    qstn-un-answered for Unanswered Question
                    qstn-bookmarked for Bookmarked Question
                */}
            </div>
            <div className="overview-legend-wrapper">
                <div className="overview-legend answered">Answered</div>
                <div className="overview-legend un-answered">Unanswered</div>
                <div className="overview-legend bookmarked">Bookmarked</div>
                <div className="overview-legend c-question">Current Question</div>
            </div>
            <div className="exam-btn-container text-right">
                <Button size="lg" className="btn-exam-submit">Submit Exam</Button>
            </div>
        </div>
    )
}

export default OverviewComponent;