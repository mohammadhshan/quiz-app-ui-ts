import React, { useState, useEffect } from 'react';
import './questions.component.scss';
import { Row, Col, Button } from 'react-bootstrap';
//import {qustn} from './questions';
import {useSelector, useDispatch} from 'react-redux';
import { prevQustn, nextQustn } from '../../actions';
import clearIcon from '../../assets/image/clear.svg';
import bookmarkIcon from '../../assets/image/bookmark.svg';
  
const QuestionsComponent = () => {

    const qustnCount = useSelector((state:any) => state.counter.currentQuestion);

    const answeredQstnArray= useSelector((state:any) => state.counter.answeredQuestion);

    const dispatch = useDispatch();

    const [isCheckedVal, setCheckedVal] = useState(null);

    dispatch({type:'GET_QUSTN'}); 

    const qustn = useSelector((state:any) => state.counter.questions);

    const objectiveChange =(e:any) =>{
        setCheckedVal(e.target.value);
        if(answeredQstnArray.indexOf(qustnCount)<0){
            dispatch({type:'AQ', value:qustnCount}); 
        }
    }

    useEffect(() => {
        setCheckedVal(null);
    }, [qustnCount]);


    return (
        <div className="col px-lg-50 py-lg-40 py-md-15 qustn-container">
            <div className="qustn-title-container">
                <div className="qustn-num">
                    Question : <span>{qustnCount}</span>
                </div>
                <div className="q-bc-container">
                    <Button size="sm"><img src={clearIcon} alt="Clear Response"/>Clear Response</Button>
                    <Button size="sm"><img src={bookmarkIcon} alt="Bookmark"/>Bookmark</Button>
                </div>
            </div>
            <div className="qustn-view-container">
                <div className="qustn-view">
                    {qustn[qustnCount-1].title}
                </div>
                <div className="qustn-obj-container">
                    <p>Options</p>
                    <div className="qustn-obj-view">
                        <Row className="choice-row mx-minus-10">
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                        <input type="radio" value="A" name="objective-ans" 
                                        onChange={(e:any)=>{objectiveChange(e)}} checked={isCheckedVal === "A"}>
                                        </input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[0].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[0].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                    <input type="radio" value="B" name="objective-ans" 
                                        onChange={(e:any)=>{objectiveChange(e)}} checked={isCheckedVal === "B"}></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[1].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[1].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                        </Row>
                        <Row className="choice-row mx-minus-10">
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                    <input type="radio" value="C" name="objective-ans" 
                                        onChange={(e:any)=>{objectiveChange(e)}} checked={isCheckedVal === "C"}></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[2].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[2].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                            <Col md={6} className="choice-col-main">
                                <div className="choice-column">
                                    <label className="choice-column-inner">
                                    <input type="radio" value="D" name="objective-ans" 
                                        onChange={(e:any)=>{objectiveChange(e)}} checked={isCheckedVal === "D"}></input>
                                        <div className="objective-ans-view">
                                            <span><i>{qustn[qustnCount-1].options[3].key}</i></span>
                                            <b>{qustn[qustnCount-1].options[3].value}</b>
                                        </div>
                                    </label>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className="qustn-desc">
                        <textarea placeholder="Description"></textarea>
                    </div>
                </div>
            </div>
            <div className="qustn-btn-wrapper">
                <div>
                    <Button variant="dark" size="sm" className="qstn-prev-btn" onClick={()=>dispatch(prevQustn())}>Previous</Button>
                </div>
                <div>
                    <Button variant="dark" size="sm" className="qstn-next-btn" onClick={()=>dispatch(nextQustn())}>Save & Next</Button>
                </div>
            </div>
        </div>
    )
}

export default QuestionsComponent;